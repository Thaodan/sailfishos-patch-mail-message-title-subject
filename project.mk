# -*- makefile -*-
NAME = "Mail Message Title"
SUMMARY = "Changes the title of the MessageView page to Email-Subject"
DESCRIPTION := "This patch changes the title of the MessageView page to Email-Subject, compatible to other patches as long MessageViewHeader isn't changed"
LICENSE=BSD
VER=0.2
MAINTAINER=Thaodan
# see https://github.com/sailfishos-patches/patchmanager#categories
CATEGORY=email
